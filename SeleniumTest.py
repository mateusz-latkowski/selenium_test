import unittest
from selenium import webdriver


class CalculatorTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://www.anaesthetist.com/mnm/javascript/calc.htm")

    def test_multiply(self):
        """..."""
        self.driver.find_element_by_name("seven").click()
        self.driver.find_element_by_name("mul").click()
        self.driver.find_element_by_name("six").click()
        self.driver.find_element_by_name("result").click()

        number = self.driver.find_element_by_name("Display").get_attribute("value")
        self.assertEqual(number, "42")

        self.driver.save_screenshot("screen.png")
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
